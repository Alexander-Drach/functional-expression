var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

console.log("Задача 1. создать два массива");

var students = studentsAndPoints.filter(function(studNames) {
  return typeof(studNames) === 'string';
});

console.log("Студенты: " + students);

var points = studentsAndPoints.filter(function(studpoints) {
  return typeof(studpoints) === 'number';
});

console.log("Баллы: " + points);

console.log("Задача 2. Вывести массив в виде: Студент Алексей Петров набрал 0 баллов");

students.forEach(function(name, i, students) {
  console.log(name);
});

points.forEach(function(point, i, points) {
  console.log(point);
});



